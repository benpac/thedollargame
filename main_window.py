import sys
import qt


import view.game_widget as gw
import view.widgets as widgets

import model.thedollargame as tdg
import model.graph as graph


class MainWindow(qt.QMainWindow):
    def __init__(self):
        super().__init__()

        self.game_widget = gw.GameWidget(self)
        self.setCentralWidget(self.game_widget)
        self.dock_widget = qt.QDockWidget(self.tr("The Dollar Game"), self)
        self.dock_widget.setMaximumSize(qt.QSize(400, 1200))
        side_layout = qt.QVBoxLayout()
        self.explanation = widgets.ExplanationWidget(self)
        self.buttons = widgets.ButtonWidgets(self, self.restart_game)
        side_layout.addWidget(self.explanation)
        side_layout.addWidget(self.buttons)
        dock_widget_widget = qt.QWidget(self)
        dock_widget_widget.setLayout(side_layout)
        self.dock_widget.setWidget(dock_widget_widget)
        self.addDockWidget(qt.Qt.RightDockWidgetArea, self.dock_widget)
        self.game = None
        # to be modifiable later
        self.n_nodes = 12
        self.n_edges = 12
        self.buttons.set_edge_and_node(self.n_nodes, self.n_edges)
        self.nodes = []
        self.bounds = (-3, 3)

        self.reinitialize()

    def get_nodes_and_edges_from_buttons(self):
        self.n_nodes = self.buttons.get_n_nodes()
        self.n_edges = self.buttons.get_n_edges()

    def reinitialize(self):
        self.get_nodes_and_edges_from_buttons()
        game_graph, nodes = graph.get_usable_graph_no_recursion(self.n_nodes, self.n_edges)
        self.nodes = nodes
        # we might have added edges to complete the graph
        self.n_edges = len(game_graph.edges())
        self.buttons.set_edge_and_node(len(self.nodes), self.n_edges)
        self.game = tdg.TheDollarGame(game_graph)
        self.game.redistribute_values(None, self.bounds)
        self.game_widget.set_graph(game_graph)
        self.game_widget.update()

    def restart_game(self):
        self.reinitialize()

    def handle_click(self, node):
        # Click actions
        if node is None:
            return
        self.game.distribute_from(node)
        # after click (is game won?)
        if self.game.check_win():
            print("WIN")
            self.reinitialize()
        self.game_widget.update()

    def redraw(self):
        pass


if __name__ == "__main__":
    app = qt.QApplication(sys.argv)
    main_window = MainWindow()
    # label = qt.QLabel("Hello World")
    main_window.show()
    sys.exit(app.exec_())
