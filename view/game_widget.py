import qt

import view.graph_drawer_helper as gdh


class GameWidget(qt.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.helper = gdh.GraphDrawerHelper()
        self.graph = None
        self.setBackgroundRole(qt.QPalette.Window)
        self.edge_color = qt.QColor(0, 0, 0, 255)
        self.node_color = qt.QColor(255, 50, 50, 255)
        self.node_color_hover = qt.QColor(50, 255, 50)
        self.node_color_hover_connected = qt.QColor(100, 100, 255)
        self.current_node_hover = None
        self.hover_width = 5
        # To force the mouseEvent to be triggered also when button are not pressed
        self.setMouseTracking(True)

        # self.setBaseSize(qt.QSize(400,400))

    def minimumSizeHint(self, *args, **kwargs):
        return qt.QSize(200, 200)

    def sizeHint(self, *args, **kwargs):
        return qt.QSize(400, 400)

    def set_graph(self, graph):
        self.graph = graph
        self.helper.set_graph(graph)

    def _get_node_radii(self):
        radii = [min(20, r/20) for r in self.size().toTuple()]
        return radii

    # region PAINT

    def paintEvent(self, event):
        painter = qt.QPainter(self)
        self.draw(painter)
        pass

    def transform_position(self, position, index=0):
        size = self.size().toTuple()
        # print(position, size)
        return 0.5 * (position + 1) * 0.8 * size[index] + size[index]/20

    def transform_back_positions(self, x, y):
        size = self.size().toTuple()
        x_t = (x - size[0] / 20) / 0.5 / 0.8 / size[0] - 1
        y_t = (y - size[1] / 20) / 0.5 / 0.8 / size[1] - 1
        return x_t, y_t

    def draw(self, painter):
        self.draw_edges (painter, self.helper.get_edges())
        self.draw_nodes (painter, self.helper.get_nodes())
        self.draw_hover(painter, self.helper.get_hover_data(self.current_node_hover))
        self.draw_values(painter, self.helper.get_values())

    def draw_edges(self, painter, edges_tuple):
        """
        :param edges_tuple: list of 2-tuple of x,y coordinates
        """
        edge_color = qt.QColor(0, 0, 0, 255)
        painter.setPen(self.edge_color)
        # print(edges_tuple)
        for edge in edges_tuple:
            painter.drawLine(self.transform_position(edge[0][0], 0),
                             self.transform_position(edge[0][1], 1),
                             self.transform_position(edge[1][0], 0),
                             self.transform_position(edge[1][1], 1))

    def draw_nodes(self, painter, node_positions):
        """
        :param node_positions: list of x,y coordinates
        """
        radii = self._get_node_radii()
        painter.setPen(self.edge_color)
        painter.setBrush(self.node_color)
        for node_position in node_positions:
            painter.drawEllipse(qt.QPointF(
                self.transform_position(node_position[0], 0),
                self.transform_position(node_position[1], 1)),
                *radii)

    def draw_values(self, painter, values_positions):
        """
        :param values_positions: list of 2 tuple ([x, y] coordinate, string-value)
        :return:
        """
        painter.setPen(self.edge_color)
        painter.setBrush(self.node_color)
        for value_position in values_positions:
            position, value = value_position
            painter.drawText(
                self.transform_position(position[0], 0),
                self.transform_position(position[1], 1),
                value)

    def draw_hover(self, painter, hover_data):
        node_hovered, neighbour_nodes, neighbour_edges = hover_data
        if node_hovered is None:
            return
        painter.setPen(self.edge_color)
        painter.setBrush(self.node_color_hover)
        painter.pen().setWidth(self.hover_width)
        for edge in neighbour_edges:
            painter.drawLine(self.transform_position(edge[0][0], 0),
                             self.transform_position(edge[0][1], 1),
                             self.transform_position(edge[1][0], 0),
                             self.transform_position(edge[1][1], 1))
        radii = self._get_node_radii()
        painter.pen().setWidth(0)
        painter.drawEllipse(qt.QPointF(
                self.transform_position(node_hovered[0], 0),
                self.transform_position(node_hovered[1], 1)),
                *radii)
        painter.setBrush(self.node_color_hover_connected)
        for node_position in neighbour_nodes:
            painter.drawEllipse(qt.QPointF(
                self.transform_position(node_position[0], 0),
                self.transform_position(node_position[1], 1)),
                *radii)

    # endregion

    # region EVENTS

    def mousePressEvent(self, event):
        # find node clicked, return it to mainwindow
        # print(event)
        x, y = self.transform_back_positions(event.x(), event.y())
        node = self.helper.get_node_at_coordinates(x, y, radius=0.1)
        self.parent().handle_click(node)

    def mouseMoveEvent(self, event):
        # print("Mouse move event", event)
        x, y = self.transform_back_positions(event.x(), event.y())
        node = self.helper.get_node_at_coordinates(x, y, radius=0.1)
        self.current_node_hover = node
        self.update()


    # endregion

