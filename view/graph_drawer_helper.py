import networkx as nx
import networkx.classes.function as nx_func
import scipy.spatial as sp
import numpy as np

class GraphDrawerHelper(object):
    def __init__(self):
        self.graph = None
        self.positions = None

    def set_graph(self, graph):
        self.graph = graph
        self.positions = nx.spring_layout(graph)
        list_nodes = list(self.graph.nodes())
        self.coords = np.dstack(([self.positions[node][0] for node in list_nodes],
                                [self.positions[node][1] for node in list_nodes]))[0]
        self.kdtree = sp.cKDTree(self.coords)

    def get_edges(self):
        assert self.graph is not None
        edges = self.graph.edges()
        edges_positions = []
        for edge in edges:
            edges_positions.append((self.positions[edge[0]], self.positions[edge[1]]))
        return edges_positions

    def get_nodes(self):
        assert self.graph is not None
        node_positions = []
        for node in self.graph.nodes():
            node_positions.append(self.positions[node])
        return node_positions

    def get_values(self):
        assert self.graph is not None
        values_positions = []
        for node in self.graph.nodes():
            values_positions.append((self.positions[node], str(node.value)))
        return values_positions

    def get_hover_data(self, hovered_node):
        assert self.graph is not None
        if hovered_node is None or hovered_node not in self.positions:
            return None, None, None
        hover_node_position = self.positions[hovered_node]
        neighbour_nodes = [n for n in nx_func.all_neighbors(self.graph, hovered_node)]
        neighbour_positions = [self.positions[n] for n in neighbour_nodes]
        neighbour_edges = [(self.positions[hovered_node], self.positions[n]) for n in neighbour_nodes]
        return hover_node_position, neighbour_positions, neighbour_edges

    def get_node_at_coordinates(self, x, y, radius):
        # print("clicked here:", x, y)
        # need to find the closest node in a radius around this point.
        dist, indexes = self.kdtree.query([(x, y)])
        # print(dist, indexes)
        dist_to_closest = dist[0]
        index_to_closest = indexes[0]
        if dist_to_closest < radius:
            list_nodes = list(self.graph.nodes())
            return list_nodes[index_to_closest]
        else:
            return None
