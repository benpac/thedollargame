import qt


class ExplanationWidget(qt.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.text = self.tr("""
This is the Dollar Game!
The objective of the game is to redistribute the amount of money each node of the graph has until every body has zero or positive amount.
Click on a node to redistribute 1 dollar to each of the connected nodes.  
        """)
        self.label = qt.QLabel(self.text)

        self.widget_layout = qt.QHBoxLayout()
        self.widget_layout.addWidget(self.label)

        self.setLayout(self.widget_layout)


class ValuedLabel(qt.QLabel):
    def __init__(self, base_text, default_value, parent=None):
        super().__init__(parent=parent)
        self.base_text = base_text
        self.value = default_value
        self.setText(self.create_text())

    def create_text(self):
        return "{}: {}".format(self.base_text, self.value)

    def set_value(self, new_value):
        self.value = new_value
        self.setText(self.create_text())


class ButtonWidgets(qt.QWidget):
    def __init__(self, parent=None, restart_button_function=None):
        super().__init__(parent)

        slider_n_nodes = qt.QSlider(qt.Qt.Horizontal)
        slider_n_nodes.setMinimum(3)
        slider_n_nodes.setMaximum(150)
        slider_n_nodes.setTickInterval(1)
        slider_n_nodes.setTracking(True)
        slider_n_nodes.valueChanged.connect(self.handle_n_nodes_changed)
        self.slider_n_nodes = slider_n_nodes
        self.n_nodes_label = ValuedLabel("Number of nodes", 3)

        slider_n_edges = qt.QSlider(qt.Qt.Horizontal)
        slider_n_edges.setMinimum(0)
        slider_n_edges.setMaximum(250)
        slider_n_edges.setTickInterval(1)
        slider_n_edges.setTracking(True)
        slider_n_edges.valueChanged.connect(self.handle_n_edges_changed)
        self.slider_n_edges = slider_n_edges
        self.n_edges_label = ValuedLabel("Number of edges", 2)

        self.button_restart = qt.QPushButton()
        self.button_restart.setText(self.tr("Restart"))
        if restart_button_function is not None:
            self.button_restart.pressed.connect(restart_button_function)

        self.button_info = qt.QPushButton()
        self.button_info.setText(self.tr("Information"))

        h_layout = qt.QVBoxLayout()
        h_layout.addWidget(self.n_nodes_label)
        h_layout.addWidget(self.slider_n_nodes)
        h_layout.addWidget(self.n_edges_label)
        h_layout.addWidget(self.slider_n_edges)
        h_layout.addWidget(self.button_restart)
        h_layout.addWidget(self.button_info)

        self.setLayout(h_layout)

    def handle_n_edges_changed(self):
        new_value = self.slider_n_edges.value()
        self.n_edges_label.set_value(new_value)

    def set_edge_and_node(self, n_nodes, n_edges):
        self.slider_n_edges.setValue(n_edges)
        self.slider_n_nodes.setValue(n_nodes)

    def handle_n_nodes_changed(self):
        new_value = self.slider_n_nodes.value()
        self.n_nodes_label.set_value(new_value)
        min_edge = new_value - 1
        max_edge = self._max_n_edges(new_value)
        self.slider_n_edges.setMinimum(min_edge)
        self.slider_n_edges.setMaximum(self._max_n_edges(new_value))
        if self.slider_n_edges.value() < min_edge:
            self.slider_n_edges.setValue(min_edge)
        if self.slider_n_edges.value() > max_edge:
            self.slider_n_edges.setValue(max_edge)

    def _max_n_edges(self, n_nodes):
        return n_nodes * (n_nodes + 1) / 2

    def get_n_nodes(self):
        return self.slider_n_nodes.value()

    def get_n_edges(self):
        return self.slider_n_edges.value()

