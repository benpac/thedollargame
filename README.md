# TheDollarGame

Inspired by the lovely video from Brady Haran's Numberphile channel: 
https://www.youtube.com/watch?v=U33dsEcKgeQ


The Dollar Game is a simple game involving connected graphs (for a more interesting situation) 
where the objective, in a very social manner, is to redistribute money until everybody is out of 
debt. 

Clicking a node will distribute wealth out of this node to all of the connected nodes. The amount
 distributed to each node is always *1*. 
 
 It is difficult to know for sure that the graph can be solved. One key element to the 
 solvability is to have a total amount of money in the game equal to *at least the graph _genus_ +
  1* (the graph genus is computed as *Number of edges* - *Number of node* + 1)



## How to play?

Download this repository and start the game with the following command:

    python main_window.py


## Note

This is currently not working properly... Will only display the starting graph!

