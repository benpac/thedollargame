import model.graph as graph
import model.thedollargame as tdg

import networkx.drawing as graph_draw
import networkx as nx
import matplotlib.pyplot as plt

seed = None

# # create vertices
# vertices = []
# for i in range(15):
#     vertices.append(graph.Vertice(i))
#
# # make edges
# # edges = {vertice: [vertices[(index+1) % len(vertices)]] for index, vertice in enumerate(vertices)}
# edges = [(vertices[(index+1) % len(vertices)], vertices[(index+2) % len(vertices)]) for index in
#          range(len(vertices))]

my_graph, vertices = graph.get_usable_graph(15, 25)

spring_layout = nx.spring_layout(my_graph, dim=2)
print(spring_layout)
for edge in my_graph.edges():
    print(edge)

my_game = tdg.TheDollarGame(my_graph)

my_game.redistribute_values(seed, (-3, 3))

my_game.distribute_from(vertices[0])

print(my_game.check_win())

print(my_game)

labels = {v: v.value for v in my_graph.nodes()}
graph_draw.draw(my_game.graph, labels=labels)
# positions = nx.spring_layout(my_graph)
#
# graph_draw.draw_networkx_labels(my_game.graph, positions, labels)
plt.show()
