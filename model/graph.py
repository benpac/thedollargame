# This class define a graph. It defines it through
import networkx as nx
import random as rng
import itertools

class Vertice(object):
    """
    A Vertice is just an object containing its value (for now)
    """
    def __init__(self, value=0, name="name"):
        self.value = value
        self.name = name

    def __str__(self):
        return "V({}, {})".format(self.value, self.name)

    def __repr__(self):
        return self.__str__()


def get_usable_graph_no_recursion(n_nodes, n_edges):
    assert n_edges >= (n_nodes-1)
    tmp_graph = nx.gnm_random_graph(n_nodes, n_edges)
    if not nx.is_connected(tmp_graph):
        connected_components = nx.connected_components(tmp_graph)
        for cc_group in itertools.combinations(connected_components, 2):
            # get a random node in each group and add edge
            # print(cc_group)
            group_1, group_2 = cc_group
            print(group_1, group_2)
            n_1 = group_1.pop()
            group_1.add(n_1)
            n_2 = group_2.pop()
            group_2.add(n_2)
            tmp_graph.add_edge(n_1, n_2)
        assert nx.is_connected(tmp_graph)
    vertices = [Vertice(name=str(i)) for i in range(n_nodes)]
    graph = nx.Graph()
    graph.add_nodes_from(vertices)
    tmp_edges = tmp_graph.edges_iter()
    for edge in tmp_edges:
        graph.add_edge(vertices[edge[0]], vertices[edge[1]])

    return graph, vertices


def get_usable_graph(n_nodes, n_edges):
    assert n_edges >= (n_nodes-1)
    vertices = [Vertice(name=i) for i in range(n_nodes)]
    graph = nx.Graph()
    graph.add_nodes_from(vertices)
    edges_integer = get_random_edges(n_nodes, n_edges)
    # print("edges, ", edges_integer)
    edges = [(vertices[edge_pair[0]], vertices[edge_pair[1]]) for edge_pair in edges_integer]
    # print("edges", edges)
    graph.add_edges_from(edges)
    if not nx.is_connected(graph):
        print("graph", graph, "is not connected")
        return get_usable_graph(n_nodes, n_edges)
    else:
        print("graph is connected")
        return graph, vertices


def get_random_pair(n_nodes):
    a = rng.randrange(n_nodes)
    b = rng.randrange(n_nodes)
    if a == b:
        return get_random_pair(n_nodes)
    else:
        return a, b


def get_random_edges(n_nodes, n_edges):
    edge_pairs = []
    while len(edge_pairs) < n_edges:
        edge_pair = get_random_pair(n_nodes)
        # print("edge_pair, ", edge_pair)
        inverted_pair = (edge_pair[1], edge_pair[0])
        if edge_pair not in edge_pairs and inverted_pair not in edge_pair:
            edge_pairs.append(edge_pair)
        # else:
        #     print("rejected")
    return edge_pairs

