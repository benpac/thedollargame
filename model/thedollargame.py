import random
import networkx.classes.function as nx_func

class TheDollarGame(object):
    """
    This class contains the logic for the dollar game.
    The Dollar Game is describe best (very subjectively) here: https://www.youtube.com/watch?v=U33dsEcKgeQ
    The basic idea is that, from a connected graph, with positive or negative values attributed
    to the vertices, the player can decide to distribute value to each connected vertex of a
    vertex. The ultimate goal is to have distributed the value across the graph so that no vertex
    has a negative value.
    """
    def __init__(self, graph):
        self.graph = graph

    def distribute_from(self, vertice):
        print("Distributing from ", vertice)
        neighbours = [n for n in nx_func.all_neighbors(self.graph, vertice)]
        n_edges = len(neighbours)
        vertice.value -= n_edges
        for vertex in neighbours:
            vertex.value += 1

    def check_win(self):
        min_value = min([v.value for v in self.graph.nodes()])
        win = min_value >= 0
        return win

    def graph_genus(self):
        n_e = len(self.graph.edges())
        n_n = len(self.graph.nodes())
        return n_e - n_n + 1

    def graph_total_value(self):
        value = 0
        for vertex in self.graph.nodes():
            value += vertex.value
        return value

    def check_genus_condition(self):
        condition = self.graph_genus() <= self.graph_total_value()
        return condition

    def redistribute_values(self, seed=None, bounds=(-3, 3)):
        random.seed(seed)
        acceptable_distribution = False
        while not acceptable_distribution:
            print("redistributing")
            for vertex in self.graph.nodes():
                vertex.value = random.randint(*bounds)
            acceptable_distribution = self.check_genus_condition() and not self.check_win()

    def __str__(self):
        nodes = [n for n in self.graph.nodes()]
        return "DollarGame({})".format(nodes)

